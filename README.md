# My Alpine Linux Dotfiles
It's a wayland/sway system with LukeSmith's i3blocks as the bar

# Dependencies
`apk add eudev font-noto seatd sway dbus lf pipewire pipewire-pulse xdg-desktop-portal-wlr`

install a graphics card driver of your own, example if you're running an amd gpu.
`apk add xf86-video-ati`

optional for running most xorg programs on wayland
`apk add xwayland`

run this command so that zsh have file to store zsh history
`mkdir -p ~/.cache/zsh`

# Auto-installation?
I'm currently making ARSE. I don't know when it will be finished, but I will be sure to finish it one day in the future.
